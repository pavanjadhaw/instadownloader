<p align="center">
  <img src="./hero.png" height="200px"/>
  <br><br>
  <b>Download all instagram posts from any profile</b>
  <br>
</p>

&nbsp;

#### why?

Instagram doesnt provide any api to easily download user images, this might be helpful for people who wants to have all the posts posted by them or their friends

### requirements

Make sure you have [nodejs](https://nodejs.org) installed

```sh
$ npm install
```

#### usage

Edit `config.js` and replace example username, password and targetUsername with real credentials

```js
/**
 * Your user credentials so our api can login and view private accounts
 */
const username = 'yourrealusernamehere';
const password = 'yourrealpasswordhere';

/**
 * Target user profile username
 * the username of profile we want to download images from
 */
const targetUsername = 'usernameofprofileyouwanttodownload';
```

Once you have provided valid credentials run the script

```sh
$ node index.js
```

&nbsp;

Have a great day!
