/**
 * external imports
 */
const puppeteer = require('puppeteer');
const mkdirp = require('mkdirp');

/**
 * utils
 */
const { downloadImage } = require('./downloader');

/**
 * import user credentials and target username
 */
const { username, password, targetUsername } = require('./config');

/**
 * paths
 */
const UPLOAD_DIR = './images';
const DOWNLOAD_PATH = `${UPLOAD_DIR}/${targetUsername}`;

// ensure images directory exists.
mkdirp.sync(UPLOAD_DIR);
mkdirp.sync(DOWNLOAD_PATH);

/**
 * main function
 */
(async () => {
  const browser = await puppeteer.launch({
    headless: true, // change to false to see browser in action
    slowMo: 0, // change to 200 to slow down browser
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });

  const page = await browser.newPage();

  const navigationPromise = page.waitForNavigation();

  /**
   * open instagram login page
   */
  await page.goto('https://www.instagram.com/accounts/login/?hl=en');

  await navigationPromise;

  await page.setViewport({ width: 1366, height: 630 });

  await page.waitForSelector(
    '.HmktE > .-MzZI:nth-child(2) > ._9GP1n > .f0n8F > ._2hvTZ',
    {
      visible: true
    }
  );
  await page.click('.HmktE > .-MzZI:nth-child(2) > ._9GP1n > .f0n8F > ._2hvTZ');
  await page.waitForSelector('.HmktE > .-MzZI > .HlU5H > .f0n8F > ._2hvTZ', {
    visible: true
  });
  await page.click('.HmktE > .-MzZI > .HlU5H > .f0n8F > ._2hvTZ');

  /**
   * enter username and password
   */
  await page.type('.HmktE > .-MzZI > .HlU5H > .f0n8F > ._2hvTZ', username);
  await page.type(
    '#react-root > section > main > div > article > div > div:nth-child(1) > div > form > div:nth-child(3) > div > label > input',
    password
  );

  /**
   * wait for login successful
   */
  await Promise.all([
    page.click(
      '#react-root > section > main > div > article > div > div:nth-child(1) > div > form > div:nth-child(4) > button'
    ),
    page.waitForNavigation({ waitUntil: 'networkidle0' })
  ]);

  /**
   * open profile of target username whose we want to download images from
   */
  await Promise.all([
    page.goto(`https://instagram.com/${targetUsername}`),
    page.waitForNavigation({ waitUntil: 'networkidle0' })
  ]);

  /**
   * select all images on user profile and get src
   */
  const imgs = await page.$$eval('img[src]', imgs =>
    imgs.map(img => img.getAttribute('src'))
  );

  /**
   * loop over and download all images one by one
   */
  for (const [index, url] of imgs.entries()) {
    console.log(`Downloading image: ${index} of ${imgs.length}`);
    await downloadImage(url, DOWNLOAD_PATH, index).then(function(data) {
      console.log('Image downloaded');
    });
  }

  await browser.close();
})();
