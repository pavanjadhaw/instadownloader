/**
 * Your user credentials so our api can login and view private accounts
 */
const username = 'myusername';
const password = 'mypassword';

/**
 * Target user profile username
 * the username of profile we want to download images from
 */
const targetUsername = 'purveshsune';

module.exports = {
  username,
  password,
  targetUsername
};
