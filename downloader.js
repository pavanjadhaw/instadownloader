/**
 * external imports
 */
const axios = require('axios');

/**
 * native imports
 */
const fs = require('fs');
const Path = require('path');

/**
 * function to download images from url
 * takes image url and name to save image with
 */
const downloadImage = async (url, dir, name) => {
  const path = Path.resolve(__dirname, dir, `${name}.png`);

  // axios image download with response type "stream"
  const response = await axios({
    method: 'GET',
    url: url,
    responseType: 'stream'
  });

  // pipe the result stream into a file on disc
  response.data.pipe(fs.createWriteStream(path));

  // return a promise and resolve when download finishes
  return new Promise((resolve, reject) => {
    response.data.on('end', () => {
      resolve();
    });

    response.data.on('error', () => {
      reject();
    });
  });
};

module.exports = {
  downloadImage
};
